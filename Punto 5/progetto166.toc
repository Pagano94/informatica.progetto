\select@language {italian}
\contentsline {chapter}{Introduzione}{1}
\contentsline {chapter}{\numberline {1}Sviluppo del progetto in R}{2}
\contentsline {section}{\numberline {1.1}Caricamento dati}{2}
\contentsline {section}{\numberline {1.2}Analisi dati}{3}
\contentsline {subsection}{\numberline {1.2.1}Funzione str()}{3}
\contentsline {subsection}{\numberline {1.2.2}Funzione names()}{3}
\contentsline {subsection}{\numberline {1.2.3}Funzione summary()}{3}
\contentsline {section}{\numberline {1.3}Analisi valori Euro}{3}
\contentsline {section}{\numberline {1.4}Specificazione dei valori}{4}
\contentsline {section}{\numberline {1.5}Rappresentazione grafica}{5}
\contentsline {chapter}{\numberline {2}R markdown}{10}
\contentsline {chapter}{\numberline {3}R presentation}{11}
\contentsline {chapter}{\numberline {4}Presentazione interattiva: Shiny}{12}
\contentsline {chapter}{\numberline {A}Equivanete di R in un foglio elettronico}{13}
\contentsline {section}{\numberline {A.1}Valore minimo}{14}
\contentsline {section}{\numberline {A.2}Valore massimo}{14}
\contentsline {section}{\numberline {A.3}Valore medio}{14}
\contentsline {section}{\numberline {A.4}Varianza}{14}
\contentsline {section}{\numberline {A.5}Numero elementi}{14}
\contentsline {section}{\numberline {A.6}Somma totale}{15}
\contentsline {section}{\numberline {A.7}Grafico lineare}{15}
\contentsline {section}{\numberline {A.8}Grafico a torta}{16}
\contentsline {chapter}{Bibliografia}{17}
