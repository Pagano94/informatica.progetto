Bilancio-Spese attivita Beni Culturali certificato preventivo 2008
========================================================
author: Vittorio Pagano
date: 19 Febbraio 2015

Caricamento dati
========================================================

In questa fase vi e' il caricamento dei dati in un oggetto _data_ dal file _progetto166.csv_.
La funzione utilizzata e' _read.csv()_.



Analisi dei dati
========================================================

Abbiamo due analisi:
* su tutti i dati attraverso le funzioni:
  + _str()_;
  + _names()_;
  + _summary()_.
* solo su i valori della colonna Euro attraerso le funzioni:
  + _min()_;
  + _max()_;
  + _mean()_;
  + _var()_;
  + _sd()_;
  + _length()_;
  + _sum()_.

Esempi di analisi 
========================================================

Ecco i valori della colonna Euro utilizzando _summary()_.


```
      Euro       
 Min.   : 14000  
 1st Qu.: 33000  
 Median : 43000  
 Mean   :149025  
 3rd Qu.:216050  
 Max.   :628548  
```

Ecco la somma totale dei valori utilizzando _sum()_.


```
[1] 1341226
```


Rappresentazione grafica
========================================================

La rappresentazione grafica permette di esprimere l'andamento in questo caso dei valori in Euro in base alle proprie esigenze. I piu' significativi sono:
* il grafico lineare rappresentante l'andamento dei valori della Colonna Euro sviluppato utilizzando la funzione _plot()_;

* il grafico a torta rappresentante le quantita' dei valori in Euro in base alle Categorie, sviluppato  utilizzando la funzione _pie()_.

=======================================================


![plot of chunk unnamed-chunk-4](progetto166-figure/unnamed-chunk-4-1.png) 



========================================================

![plot of chunk unnamed-chunk-5](progetto166-figure/unnamed-chunk-5-1.png) 

